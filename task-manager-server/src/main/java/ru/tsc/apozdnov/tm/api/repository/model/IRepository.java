package ru.tsc.apozdnov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    void set(@NotNull Collection<M> models);

    void update(@NotNull M model);

    @Nullable
    M findOneById(@NotNull String id);

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    void clear();

    long getCount();

    boolean existsById(@NotNull String id);

    @NotNull
    List<M> findAll();

    @NotNull
    EntityManager getEntityManager();

}