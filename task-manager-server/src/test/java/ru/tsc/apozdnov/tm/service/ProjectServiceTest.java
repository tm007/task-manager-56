package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.apozdnov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.apozdnov.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.apozdnov.tm.config.ContextConfig;
import ru.tsc.apozdnov.tm.dto.model.ProjectDTO;
import ru.tsc.apozdnov.tm.dto.model.UserDTO;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exception.field.EmptyDescriptionException;
import ru.tsc.apozdnov.tm.exception.field.EmptyIdException;
import ru.tsc.apozdnov.tm.exception.field.EmptyNameException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;
import ru.tsc.apozdnov.tm.util.DateUtil;

import java.util.List;
import java.util.UUID;

public class ProjectServiceTest {

    @NotNull
    private IProjectServiceDTO projectService;

    @NotNull
    private IUserServiceDTO userService;

    private String USER_ID;

    private long INITIAL_SIZE;

    private String PROJECT_ID;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ContextConfig.class);
        projectService = context.getBean(IProjectServiceDTO.class);
        userService = context.getBean(IUserServiceDTO.class);
        @NotNull final UserDTO user = userService.create("user", "user");
        USER_ID = user.getId();
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "test-1");
        INITIAL_SIZE = projectService.getCount();
        PROJECT_ID = project.getId();
    }

    @After
    public void end() {
        projectService.clear(USER_ID);
        userService.removeByLogin("user");
    }

    @Test
    public void create() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.create("", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> projectService.create(USER_ID, ""));
        projectService.create(USER_ID, "test");
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getCount());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(EmptyUserIdException.class, () -> projectService.create("", "test", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> projectService.create(USER_ID, "", "test"));
        Assert.assertThrows(EmptyDescriptionException.class, () -> projectService.create(USER_ID, "test", ""));
        projectService.create(USER_ID, "test", "test");
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getCount());
    }

    @Test
    public void createWithDescriptionAndDate() {
        @Nullable final ProjectDTO project = projectService.create(
                USER_ID,
                "test",
                "test",
                DateUtil.toDate("10.10.2021"),
                DateUtil.toDate("11.11.2021")
        );
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getCount());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getDateBegin());
        Assert.assertNotNull(project.getDateEnd());
    }

    @Test
    public void clear() {
        projectService.clear();
        Assert.assertEquals(0, projectService.getCount());
    }

    @Test
    public void findAll() {
        @NotNull final List<ProjectDTO> projectsAll = projectService.findAll();
        Assert.assertEquals(INITIAL_SIZE, projectsAll.size());
        @NotNull final List<ProjectDTO> projectsOwnedUser1 = projectService.findAll(USER_ID);
        Assert.assertEquals(1, projectsOwnedUser1.size());
        @NotNull final List<ProjectDTO> projectsOwnedUser3 = projectService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, projectsOwnedUser3.size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectService.updateById("", PROJECT_ID, "test", "test"));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectService.updateById(USER_ID, "", "test", "test"));
        Assert.assertThrows(EmptyNameException.class,
                () -> projectService.updateById(USER_ID, PROJECT_ID, "", "test"));
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        projectService.updateById(USER_ID, PROJECT_ID, newName, newDescription);
        @NotNull final ProjectDTO project = projectService.findOneById(PROJECT_ID);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectService.changeStatusById("", PROJECT_ID, newStatus));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectService.changeStatusById(USER_ID, "", newStatus));
        projectService.changeStatusById(USER_ID, PROJECT_ID, newStatus);
        @NotNull final ProjectDTO project = projectService.findOneById(PROJECT_ID);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(newStatus, project.getStatus());
    }

    @Test
    public void findOneById() {
        @NotNull final String projectName = "test find by id";
        @NotNull final ProjectDTO project = projectService.create(USER_ID, projectName);
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(EmptyIdException.class, () -> projectService.findOneById(""));
        Assert.assertNotNull(projectService.findOneById(projectId));
        Assert.assertEquals(projectName, projectService.findOneById(projectId).getName());
        Assert.assertNotNull(projectService.findOneById(USER_ID, projectId));
        Assert.assertEquals(projectName, projectService.findOneById(USER_ID, projectId).getName());
    }

    @Test
    public void existsById() {
        @NotNull final String projectName = "test exist by id";
        @NotNull final ProjectDTO project = projectService.create(USER_ID, projectName);
        @NotNull final String projectId = project.getId();
        Assert.assertTrue(projectService.existsById(projectId));
        Assert.assertFalse(projectService.existsById(UUID.randomUUID().toString()));
    }

    @Test
    public void remove() {
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "test");
        @NotNull final String projectId = project.getId();
        projectService.remove(project);
        Assert.assertEquals(INITIAL_SIZE, projectService.getCount());
        projectService.add(project);
        projectService.remove(USER_ID, project);
        Assert.assertEquals(INITIAL_SIZE, projectService.getCount());
    }

    @Test
    public void removeById() {
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "test");
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(EmptyIdException.class, () -> projectService.removeById(""));
        projectService.removeById(projectId);
        Assert.assertEquals(INITIAL_SIZE, projectService.getCount());
        projectService.add(project);
        Assert.assertThrows(EmptyIdException.class, () -> projectService.removeById(USER_ID, ""));
        projectService.removeById(USER_ID, projectId);
        Assert.assertNull(projectService.findOneById(USER_ID, UUID.randomUUID().toString()));
        Assert.assertEquals(INITIAL_SIZE, projectService.getCount());
    }

}