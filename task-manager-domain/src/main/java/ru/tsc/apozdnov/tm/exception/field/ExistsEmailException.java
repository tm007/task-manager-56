package ru.tsc.apozdnov.tm.exception.field;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class ExistsEmailException extends AbstractException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}