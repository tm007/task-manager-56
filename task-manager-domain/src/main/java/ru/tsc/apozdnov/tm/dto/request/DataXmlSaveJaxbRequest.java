package ru.tsc.apozdnov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataXmlSaveJaxbRequest extends AbstractUserRequest {

    public DataXmlSaveJaxbRequest(@Nullable String token) {
        super(token);
    }

}