package ru.tsc.apozdnov.tm.command.system;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.api.model.ICommand;
import ru.tsc.apozdnov.tm.api.service.ICommandService;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.command.AbstractCommand;
import ru.tsc.apozdnov.tm.enumerated.Role;

@Getter
@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @Autowired
    private ICommandService commandService;

    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}