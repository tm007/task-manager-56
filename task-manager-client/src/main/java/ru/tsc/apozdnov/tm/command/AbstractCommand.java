package ru.tsc.apozdnov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.api.model.ICommand;
import ru.tsc.apozdnov.tm.api.service.ITokenService;
import ru.tsc.apozdnov.tm.enumerated.Role;

@Getter
@Setter
@Component
public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    public abstract void execute();

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    protected String getToken() {
        return getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getTokenService().setToken(token);
    }

    @Override
    public String toString() {
        String result = "";
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}